class Curso < ActiveRecord::Migration[6.0]
  def self.up
    create_table  :curso do |t|
      t.column  :title, :string, :limit => 32, :null => false
      t.column  :fee, :float
      t.column  :duration, :integer
      t.column  :index, :string
      t.column  :created_at, :timestamp
    end
  end


  def self.down
    drop_table  :curso
  end
end
